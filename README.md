# Brave Browser

Brave Browser - Secure, Fast & Private Web Browser with Adblocker

## HomePage

[Brave Browser](https://brave.com)

## Download

Download **brave-browser-x86_64.AppImage** binary from here: [brave-browser-x86_64.AppImage](https://gitlab.com/linuxappimage/brave-browser/-/jobs/artifacts/main/raw/brave-browser-x86_64.AppImage?job=run-build)

## CLI

or from you command line:

```bash
curl -sLo brave-browser-x86_64.AppImage https://gitlab.com/linuxappimage/brave-browser/-/jobs/artifacts/main/raw/brave-browser-x86_64.AppImage?job=run-build

chmod +x brave-browser-x86_64.AppImage
./brave-browser-x86_64.AppImage

```
